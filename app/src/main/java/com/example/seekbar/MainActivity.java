package com.example.seekbar;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.SeekBar;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private SeekBar sbFilas, sbColumnas, sbElementos;
    private TextView tvFila, tvColumna, tvElemento;
    private int filas, columnas, elementos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        cargarViews();
        filas = columnas = 3;
        elementos = 2;
        ponerFilas();
        ponerColumnas();
        ponerElementos();
        ponerListeners();
    }

    public void cargarViews(){
        sbFilas = (SeekBar) findViewById(R.id.sbFilas);
        sbColumnas = (SeekBar) findViewById(R.id.sbColumnas);
        sbElementos = (SeekBar) findViewById(R.id.sbElementos);
        tvFila = (TextView) findViewById(R.id.tvFila);
        tvColumna = (TextView) findViewById(R.id.tvColumna);
        tvElemento = (TextView) findViewById(R.id.tvElemento);
    }

    //----------------------------------------------------------------------------------------------

    public void ponerFilas(){
        tvFila.setText(String.format(getResources().getString(R.string.tvFila),filas));
    }

    public void ponerColumnas(){
        tvColumna.setText(String.format(getResources().getString(R.string.tvColumna),columnas));
    }

    public void ponerElementos(){
        tvElemento.setText(String.format(getResources().getString(R.string.tvElemento),elementos));
    }

    //----------------------------------------------------------------------------------------------

    public void ponerListeners() {
        sbFilas.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                filas = seekBar.getProgress()+3;
                ponerFilas();
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        sbColumnas.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                columnas = seekBar.getProgress()+3;
                ponerColumnas();
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        sbElementos.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                elementos = seekBar.getProgress()+2;
                ponerElementos();
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
    }
}
